<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::resource('room-type', 'RoomTypeController');
Route::get('room_type/{department}', 'RoomTypeController@destroy')->name('room-type.delete');

Route::resource('payment', 'PaymentController');
Route::get('payment_destroy/{department}', 'PaymentController@destroy')->name('payment.delete');

Route::resource('room-setup', 'RoomSetupController');
Route::get('room_setup/{department}', 'RoomSetupController@destroy')->name('room-setup.delete');

Route::resource('booking', 'BookingController');
Route::get('booking_delete/{department}', 'BookingController@destroy')->name('booking.delete');
Route::get('booking-schedule', 'BookingController@schedule')->name('booking.schedule');
Route::get('new-booking', 'BookingController@NewBook')->name('new_booking');
Route::get('room-rent', 'BookingController@RoomRent')->name('room.rent');

Route::resource('booking-request', 'BookingRequestController');
Route::get('booking-request/{data}/{data2}', 'BookingRequestController@accept')->name('booking.accept');
Route::get('booking-reject/{data}', 'BookingRequestController@reject')->name('booking.reject');
Route::get('booking_request_delete/{department}', 'BookingRequestController@destroy')->name('booking.request.delete');

