@extends('layout.app')
@section('title','Army Golf Club | Department Management')
@section('content')
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="fa fa-times mx-2"></i>
                <strong>Error!</strong> {{$error}}!
            </div>
        @endforeach
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> {{ session()->get('message') }}!
        </div>
    @endif
    <div class="main-content-container container-fluid px-4 mb-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">BOOKING Schedule</span>
                <h3 class="page-title">Calender
                </h3>
            </div>
            <div class="col-12 col-sm-6 d-flex align-items-center">
                <div class="d-inline-flex mb-sm-0 mx-auto ml-sm-auto mr-sm-0" role="group" aria-label="Page actions">
                    <a href="{{route('new_booking')}}" class="btn btn-secondary">
                        <i class="fas fa-plus"></i> Guest Room Booking </a>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <!-- Calendar -->
        <div class="card">
            <div class="card-body py-4">
                <div id='calendar'></div>
            </div>
        </div>
        <!-- End Calendar -->
    </div>
@endsection
@push('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.css"/>
    <style>
        .fc-content {
            cursor: pointer;
        }
    </style>
@endpush
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.js"></script>
    <script>
        "use strict";
        !function (e) {
            e(document).ready(function () {
                e("#calendar").fullCalendar({
                    locale: 'bd',
                    eventRender: function(eventObj, $el) {
                        $el.popover({
                            title: eventObj.title,
                            content: eventObj.description,
                            trigger: 'hover',
                            html: true,
                            placement: 'top',
                            container: 'body'
                        });
                    },
                    events: [
                            @foreach($booking as $bookings)
                        {
                            title: '{{"Booking No:".$bookings->booking_no}}',
                            start: '{{$bookings->booking_from}}',
                            end: '{{$bookings->booking_to."T23:59:00"}}',
                            description: '{{"Name: ".$bookings->name}} <br> {{"Room Qty: ".$bookings->room_qty}} <br> {{"Room Type: ".$bookings->room_type->room_type}} <br> {{"Mobile No: ".$bookings->cell_no}} <br> {{"Email: ".$bookings->email}}',
                            id: {{$bookings->id}}
                        },
                        @endforeach
                    ],
                    firstDay: 6,
                    displayEventTime: false,
                    header: {left: "month,listWeek", center: "title", right: "prev,next"},
                    plugins: ['dayGrid', 'timeGrid', 'list'],
                    eventClick: function(info) {
                        if (info.id) {
                            var url = '{{ route("booking.edit", ":id","edit") }}';
                            url = url.replace(':id',info.id);
                            window.open(url);
                        }
                    }
                })
            })
        }(jQuery);
    </script>
@endpush
