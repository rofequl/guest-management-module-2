@extends('layout.app')
@section('title','Army Golf Club | Department Management')
@section('content')
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="fa fa-check mx-2"></i>
                <strong>Error!</strong> {{$error}}!
            </div>
        @endforeach
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> {{ session()->get('message') }}!
        </div>
    @endif
    <div class="main-content-container container-fluid px-4 mb-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">BOOKING</span>
                <h3 class="page-title">Booking History</h3>
            </div>
            <div class="col-12 col-sm-6 d-flex align-items-center">
                <div class="d-inline-flex mb-sm-0 mx-auto ml-sm-auto mr-sm-0" role="group" aria-label="Page actions">
                    <a href="{{route('new_booking')}}" class="btn btn-primary">
                        <i class="material-icons">add</i> New Payment System Add </a>
                </div>
                <button class="ml-2 btn btn-success float-right printMe">
                    <i class="fa fa-print"> Print</i>
                </button>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="printTable2">
            <table class="transaction-history d-none">
                <thead>
                <tr>
                    <th scope="col" class="border-0">#</th>
                    <th scope="col" class="border-0">Member Id</th>
                    <th scope="col" class="border-0">Booking No</th>
                    <th scope="col" class="border-0">Room Type</th>
                    <th scope="col" class="border-0">Booking From</th>
                    <th scope="col" class="border-0">Booking To</th>
                    <th scope="col" class="border-0">Qty</th>
                    <th scope="col" class="border-0">Amount</th>
                    <th scope="col" class="border-0">Action</th>
                </tr>
                </thead>
                <tbody>
                @php $sl=1 @endphp
                @foreach($booking as $bookings)
                    <tr>
                        <td>{{$sl++}}</td>
                        <td>{{$bookings->member_id}}</td>
                        <td>{{$bookings->booking_no}}</td>
                        <td>{{$bookings->room_type->room_type}}</td>
                        <td>{{$bookings->booking_from}}</td>
                        <td>{{$bookings->booking_to}}</td>
                        <td>{{$bookings->room_qty}}</td>
                        <td>{{$bookings->total_amount}}</td>
                        <td>
                            <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                                <button type="button" class="btn btn-white edit"
                                        href="{{route('booking.edit',$bookings->id,'edit')}}">
                                    <i class="fas fa-eye"></i>
                                </button>
                                <button type="button" class="btn btn-white delete"
                                        href="{{route('booking.delete',$bookings->id)}}">
                                    <i class="material-icons">&#xE872;</i>
                                </button>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- End Transaction History Table -->
    </div>
@endsection
@push('style')
    <link rel="stylesheet" href="{{asset('assets/styles/responsive.dataTables.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/sweetalert/sweetalert.css')}}"/>
@endpush
@push('script')
    <script src="{{asset('assets/scripts/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/scripts/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/scripts/app/app-transaction-history.1.3.1.min.js')}}"></script>
    <script src="{{asset('assets/sweetalert/sweetalert.js')}}"></script>
    <script src="{{asset('assets/printThis.js')}}"></script>

    <script>
        $('.printMe').on("click", function () {
            $('.printTable2').printThis({
                base: "https://jasonday.github.io/printThis/"
            });
        });

        $('.delete').click(function (e) {
            e.preventDefault();
            let linkURL = $(this).attr("href");
            swal({
                title: "Sure want to delete?",
                text: "If you click 'OK' file will be deleted",
                type: "warning",
                showCancelButton: true
            }, function () {
                window.location.href = linkURL;
            });
        });

        $('.edit').click(function (e) {
            let linkURL = $(this).attr("href");
            window.location.href = linkURL;
        });

    </script>
@endpush
