@extends('layout.app')
@section('title','Army Golf Club | Department Management')
@section('content')
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="fa fa-times mx-2"></i>
                <strong>Error!</strong> {{$error}}!
            </div>
        @endforeach
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> {{ session()->get('message') }}!
        </div>
    @endif
    <div class="main-content-container container-fluid px-4 mb-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">BOOKING</span>
                <h3 class="page-title">Add New Booking
                </h3>
            </div>
            <div class="col-12 col-sm-6 d-flex align-items-center">
                <div class="d-inline-flex mb-sm-0 mx-auto ml-sm-auto mr-sm-0" role="group" aria-label="Page actions">
                    <a href="{{route('booking.index')}}" class="btn btn-secondary">
                        <i class="fas fa-hand-point-left"></i> Back to Booking Transaction List </a>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row">
            <div class="col-lg-4">
                <div v-if="!roomTypesInfo" class="card card-small mb-4 pt-3">
                    <div class="card-header border-bottom text-center">
                        <h4 class="mb-0">Room Setup</h4>
                        <span class="text-muted d-block mb-2">Select the button, setup new room.</span>
                        <a href="{{route('room-setup.index')}}"
                           class="mb-2 btn btn-sm btn-pill btn-outline-primary mr-2"><i
                                class="material-icons">&#xE917;</i> Room Setup
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-8">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Booking Information</h6>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-3">
                            <div class="row">
                                <div class="col">
                                    <form method="post" action="{{route('booking.store')}}" autocomplete="off">
                                        @csrf
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <input type="text" name="date" class="form-control datePick" readonly
                                                       required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input type="text" class="form-control" name="booking_no"
                                                       id="booking_no" value="{{$booking_id}}"
                                                       placeholder="Booking No" readonly required>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-row">
                                            <div class="col mb-3">
                                                <p class="form-text text-muted m-0">Check Availability</p>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <input type="text" name="booking_from" id="search_checkin"
                                                       data-datepicker="separateRange"
                                                       class="form-control" placeholder="Booking for Days, From"
                                                       required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input type="text" name="booking_to" id="search_checkout"
                                                       data-datepicker="separateRange"
                                                       class="form-control" placeholder="Booking for Days, To" required>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <select class="form-control" id="room_type_id" name="room_type_id"
                                                        required>
                                                    <option value="" selected disabled>Choose Room Type</option>
                                                    @foreach($room_type as $room_types)
                                                        <option
                                                            value="{{$room_types->id}}">
                                                            {{$room_types->room_type}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-row">
                                            <div class="col mb-3">
                                                <p class="form-text text-muted m-0">Apply for Booking
                                                    <span id="alert_text3"
                                                          style=" text-align:center; color:red; font-weight:800; font-size:15px;"></span>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="form-check custom-radio form-check-inline">
                                            <input class="custom-control-input" type="radio"
                                                   name="member_edit" id="inlineRadio1" value="0" checked>
                                            <label class="custom-control-label" for="inlineRadio1">Member</label>
                                        </div>
                                        <div class="form-check custom-radio form-check-inline">
                                            <input class="custom-control-input" type="radio"
                                                   name="member_edit" id="inlineRadio2" value="1">
                                            <label class="custom-control-label" for="inlineRadio2">Not Member</label>
                                        </div>
                                        <div class="form-row mt-2">
                                            <div class="form-group col-md-6" id="member_id">
                                                <input type="text" class="form-control"
                                                       id="cust_id" name="member_id" placeholder="Member Id" required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input type="text" class="form-control" id="user_id"
                                                       name="name" placeholder="Member Name" required>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <input type="text" class="form-control" id="mobile_number"
                                                       name="cell_no" placeholder="Member Cell No" required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input type="text" class="form-control" id="email"
                                                       name="email" placeholder="Member Email" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="mem_addres"
                                                   name="address" placeholder="Member Address" required>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <select class="form-control" name="purpose" required>
                                                    <option value="" selected="" disabled>Select Purpose</option>
                                                    <option value="Official">Official</option>
                                                    <option value="Personal">Personal</option>
                                                    <option value="Ceremony">Ceremony</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input type="number" class="form-control" id="no_guest"
                                                       name="no_guest" placeholder="No of Guests" required>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-row">
                                            <div class="col mb-3">
                                                <p class="form-text text-muted m-0">Payment</p>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <input type="number" class="form-control" id="room_rent"
                                                       name="room_rent"
                                                       placeholder="Room Rent" readonly required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input type="number" class="form-control" id="room_qty"
                                                       name="room_qty" min="1"
                                                       placeholder="No of Room" required>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <input type="number" class="form-control" id="total_amount"
                                                       name="total_amount"
                                                       placeholder="Total Amount" readonly required>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <select class="form-control"
                                                        name="payment_mood" required>
                                                    <option value="" selected="" disabled>Choose Payment Method</option>
                                                    @foreach($payment as $payments)
                                                        <option
                                                            value="{{$payments->id}}">
                                                            {{$payments->name}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-accent">Booking
                                            Submit
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Transaction History Table -->
    </div>
@endsection
@push('style')
    <link rel="stylesheet" href="{{asset('assets/styles/responsive.dataTables.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/sweetalert/sweetalert.css')}}"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.24/daterangepicker.min.css" rel="stylesheet"/>
@endpush
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.24/moment.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.24/daterangepicker.min.js"></script>
    <script src="{{asset('assets/scripts/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/scripts/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/scripts/app/app-transaction-history.1.3.1.min.js')}}"></script>
    <script src="{{asset('assets/sweetalert/sweetalert.js')}}"></script>
    <script src="{{asset('assets/printThis.js')}}"></script>

    <script>
        $(function () {
            $('.datePick').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1901,
                locale: {
                    format: 'DD-MM-YYYY'
                },
                maxYear: parseInt(moment().format('YYYY'), 10)
            });
        });

        var separator = ' - ', dateFormat = 'DD-MM-YYYY';
        var options = {
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                format: dateFormat,
                separator: separator,
                applyLabel: '確認',
                cancelLabel: '取消'
            },
            minDate: moment(),
            maxDate: moment().add(359, 'days'),
            opens: "right"
        };


        $('[data-datepicker=separateRange]')
            .daterangepicker(options)
            .on('apply.daterangepicker', function (ev, picker) {
                var boolStart = this.name.match(/booking_from/g) == null ? false : true;
                var boolEnd = this.name.match(/booking_to/g) == null ? false : true;

                var mainName = this.name.replace('booking_from', '');
                if (boolEnd) {
                    mainName = this.name.replace('value_from_end_', '');
                    $(this).closest('form').find('[name=booking_to' + mainName + ']').blur();
                }

                $(this).closest('form').find('[name=booking_from' + mainName + ']').val(picker.startDate.format(dateFormat));
                $(this).closest('form').find('[name=booking_to' + mainName + ']').val(picker.endDate.format(dateFormat));

                $(this).trigger('change').trigger('keyup');
                total_calculate();
            })
            .on('show.daterangepicker', function (ev, picker) {
                var boolStart = this.name.match(/booking_from/g) == null ? false : true;
                var boolEnd = this.name.match(/booking_to/g) == null ? false : true;
                var mainName = this.name.replace('booking_from', '');
                if (boolEnd) {
                    mainName = this.name.replace('booking_to', '');
                }

                var startDate = $(this).closest('form').find('[name=booking_from' + mainName + ']').val();
                var endDate = $(this).closest('form').find('[name=booking_to' + mainName + ']').val();

                $('[name=daterangepicker_start]').val(startDate).trigger('change').trigger('keyup');
                $('[name=daterangepicker_end]').val(endDate).trigger('change').trigger('keyup');

                if (boolEnd) {
                    $('[name=daterangepicker_end]').focus();
                }
                total_calculate();
            });// End Daterange Picker

        $("input[name=member_edit]").on("click", function () {
            let data = $(this).val();
            if (data == "1") {
                $("#member_id").hide();
            } else {
                $("#member_id").show();
            }
        });

        $(document).ready(function () {
            $('#cust_id').on('blur', function (e) {
                e.preventDefault();
                var cust_id = $('#cust_id').val();
                $("#loader").css('display', 'block');
                $this = this;
                $.ajax({
                    type: 'get',
                    dataType: 'json',
                    url: "http://gcms.issit.org/facility/member_info",
                    data: {cust_id: cust_id},
                    success: function (data) {
                        if (data.member_data != null) {
                            $('#alert_text3').text(' ');
                            $('#user_id').val(data.member_data.member_all_info.full_name);
                            $('#mobile_number').val(data.member_data.member_all_info.mobile_number);
                            $('#email').val(data.member_data.member_all_info.email);
                            $('#mem_addres').val(data.member_data.member_all_info.present_address);
                        } else {
                            $('#alert_text3').text('There is no Member exist!');
                            $('#user_id').val(' ');
                            $('#mobile_number').val(' ');
                            $('#email').val(' ');
                            $('#mem_addres').val(' ');

                        }
                    }
                });
            });
        });

        $("#room_type_id").change(function () {
            var room_type_id = $('#room_type_id').val();
            $.ajax({
                type: 'get',
                dataType: 'json',
                url: "{{route('room.rent')}}",
                data: {room_type_id: room_type_id},
                success: function (data) {
                    var room_rent = $('#room_rent').val(data.rent);
                    total_calculate();
                }
            });
        });

        $("#room_qty").keyup(function () {
            total_calculate();
        });

        function total_calculate() {
            var room_rent = $('#room_rent').val();
            var room_qty = $('#room_qty').val();
            var dateDifferences = moment.duration(moment($('#search_checkout').val(), 'DD-MM-YYYY', true)
                .diff(moment($('#search_checkin').val(), 'DD-MM-YYYY', true))).asDays() + 1;
            $('#total_amount').val(room_rent * room_qty * dateDifferences);
        }


    </script>
@endpush
