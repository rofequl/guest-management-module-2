@extends('layout.app')
@section('title','Army Golf Club | Department Management')
@section('content')
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="fa fa-check mx-2"></i>
                <strong>Error!</strong> {{$error}}!
            </div>
        @endforeach
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> {{ session()->get('message') }}!
        </div>
    @endif
    <div class="main-content-container container-fluid px-4 mb-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">BOOKING</span>
                <h3 class="page-title">Booking Details</h3>
            </div>
            <div class="col-12 col-sm-6 d-flex align-items-center">
                <div class="d-inline-flex mb-sm-0 mx-auto ml-sm-auto mr-sm-0" role="group" aria-label="Page actions">
                    <a href="{{route('booking.index')}}" class="btn bg-secondary rounded text-white text-center p-2">
                        <i class="fas fa-hand-point-left"></i> Back to Booking Transaction List </a>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row printTable">
            <div class="col-sm-12 mb-4">
                <div class="card card-body" style="padding:0;">
                    <div class="card card-post card-post--aside card-post--1" style="box-shadow:none;">
                        <div class="card-post__image" style="width: 450px!important;border-bottom-left-radius: 0;
                            background-image: url('{{asset("storage/room/".$edit->room_info->image)}}')">
                            <a href="#"
                               class="card-post__category badge badge-pill badge-info">{{$edit->room_type->room_type}}</a>

                        </div>
                        <div class="card-body">
                            <h5 class="card-title">
                                <a class="text-fiord-blue" href="#">Room Information</a>
                                <button class="btn btn-success float-right printMe">
                                    <i class="fa fa-print"> Print</i>
                                </button>
                            </h5>
                            <p class="card-text d-inline-block mb-3">
                                Room Rent: {{$edit->room_info->rent}} <br>
                                Total Room: {{$edit->room_info->qty}}<br>
                                Details: {{$edit->room_info->details}} <br>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="card-body">
                                <h5 class="card-title">
                                    <a class="text-fiord-blue" href="#">Apply for Booking</a>
                                </h5>
                                <table class="table" style="font-size:13px">
                                    <tr>
                                        <td class="border-0 p-0">
                                            <p class="card-text d-inline-block mb-1">Member Id: {{$edit->member_id}}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="border-0 p-0">
                                            <p class="card-text d-inline-block mb-1">Member Name: {{$edit->name}}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="border-0 p-0">
                                            <p class="card-text d-inline-block mb-1">Member Cell
                                                No: {{$edit->cell_no}}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="border-0 p-0">
                                            <p class="card-text d-inline-block mb-1">Member Email: {{$edit->email}}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="border-0 p-0">
                                            <p class="card-text d-inline-block mb-1">Member
                                                Address: {{$edit->address}}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="border-0 p-0">
                                            <p class="card-text d-inline-block mb-1">Select
                                                Purpose: {{$edit->purpose}}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="border-0 p-0">
                                            <p class="card-text d-inline-block mb-1">No of
                                                Guests: {{$edit->no_guest}}</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="card-body pb-0">
                                <h5 class="card-title">
                                    <a class="text-fiord-blue" href="#">Booking Info</a>
                                </h5>
                                <table class="table" style="font-size:13px">
                                    <tr>
                                        <td class="border-0 p-0">
                                            <p class="card-text d-inline-block mb-1">Date: {{$edit->date}}
                                            </p>
                                        </td>
                                        <td class="border-0 p-0">
                                            <p class="card-text d-inline-block mb-1">From
                                                Days: {{$edit->booking_from}}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="border-0 p-0">
                                            <p class="card-text d-inline-block mb-1">Booking
                                                No: {{$edit->booking_no}}</p>
                                        </td>
                                        <td class="border-0 p-0">
                                            <p class="card-text d-inline-block mb-1">To Days: {{$edit->booking_to}}</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="card-body pt-0">
                                <h5 class="card-title">
                                    <a class="text-fiord-blue" href="#">Payment</a>
                                </h5>
                                <table class="table" style="font-size:13px">
                                    <tr>
                                        <td class="border-0 p-0">
                                            <p class="card-text d-inline-block mb-1">No of Room: {{$edit->room_qty}}</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="border-0 p-0">
                                            <p class="card-text d-inline-block mb-1">Payment
                                                Method: {{$edit->payment?$edit->payment->name:''}}</p>
                                        </td>
                                        <td class="border-0 p-0">
                                            <p class="card-text d-inline-block mb-1">Total
                                                Amount: {{$edit->total_amount}}</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- End Transaction History Table -->
        </div>
        @endsection
        @push('style')
            <link rel="stylesheet" href="{{asset('assets/styles/responsive.dataTables.min.css')}}"/>
            <link rel="stylesheet" href="{{asset('assets/sweetalert/sweetalert.css')}}"/>
        @endpush
        @push('script')
            <script src="{{asset('assets/scripts/jquery.dataTables.min.js')}}"></script>
            <script src="{{asset('assets/scripts/dataTables.responsive.min.js')}}"></script>
            <script src="{{asset('assets/scripts/app/app-transaction-history.1.3.1.min.js')}}"></script>
            <script src="{{asset('assets/sweetalert/sweetalert.js')}}"></script>
            <script src="{{asset('assets/printThis.js')}}"></script>

            <script>
                $('.printMe').on("click", function () {
                    $('.printTable').printThis({
                        base: "https://jasonday.github.io/printThis/"
                    });
                });

            </script>
    @endpush
