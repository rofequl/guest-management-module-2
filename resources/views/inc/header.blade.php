<div class="main-navbar sticky-top bg-white">
    <!-- Main Navbar -->
    <nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">

        <ul class="navbar-nav border-left flex-row ">

        </ul>
        <nav class="nav">
            <a href="#"
               class="nav-link nav-link-icon toggle-sidebar d-sm-inline d-md-none text-center border-left"
               data-toggle="collapse" data-target=".header-navbar" aria-expanded="false"
               aria-controls="header-navbar">
                <i class="material-icons">&#xE5D2;</i>
            </a>
        </nav>
    </nav>
</div> <!-- / .main-navbar -->
