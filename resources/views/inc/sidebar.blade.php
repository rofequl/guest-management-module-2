<!-- Main Sidebar -->
<aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0" style="z-index: 1040">
    <div class="main-navbar">
        <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
            <a class="navbar-brand w-100 mr-0" href="{{route('home')}}" style="line-height: 25px;">
                <div class="d-table m-auto">
                    <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 25px;"
                         src="{{asset('assets/images/logo.jpg')}}" alt="Store Inventory">
                    <span class="d-none d-md-inline ml-1">Guest Room Man.</span>
                </div>
            </a>
            <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
                <i class="material-icons">&#xE5C4;</i>
            </a>
        </nav>
    </div>
    <form action="#" class="main-sidebar__search w-100 border-right d-sm-flex d-md-none d-lg-none">
        <div class="input-group input-group-seamless ml-3">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fas fa-search"></i>
                </div>
            </div>
            <input class="navbar-search form-control" type="text" placeholder="Search for something..."
                   aria-label="Search">
        </div>
    </form>
    <div class="nav-wrapper">
        <ul class="nav nav--no-borders flex-column">
            <li class="nav-item">
                <a class="nav-link" href="{{route('home')}}">
                    <i class="fas fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('booking-request.index')}}">
                    <i class="material-icons">&#xE917;</i>
                    <span>Booking Request List</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('booking.index')}}">
                    <i class="material-icons">&#xE917;</i>
                    <span>Booking</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('booking.schedule')}}">
                    <i class="material-icons">&#xE917;</i>
                    <span>Booking Schedule</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('payment.index')}}">
                    <i class="material-icons">&#xE917;</i>
                    <span>Payment</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('room-type.index')}}">
                    <i class="material-icons">&#xE917;</i>
                    <span>Room Type</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('room-setup.index')}}">
                    <i class="material-icons">&#xE917;</i>
                    <span>Room Setup</span>
                </a>
            </li>
        </ul>
    </div>
</aside>
<!-- End Main Sidebar -->
