@extends('layout.app')
@section('title','Army Golf Club | Department Management')
@section('content')
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="fa fa-check mx-2"></i>
                <strong>Error!</strong> {{$error}}!
            </div>
        @endforeach
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> {{ session()->get('message') }}!
        </div>
    @endif
    <div class="main-content-container container-fluid px-4 mb-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">BOOKING</span>
                <h3 class="page-title">Booking Request</h3>
            </div>
            <div class="col-12 col-sm-6 d-flex align-items-center">
                <div class="d-inline-flex mb-sm-0 mx-auto ml-sm-auto mr-sm-0" role="group" aria-label="Page actions">
                    <button class="ml-2 btn btn-success float-right printMe">
                        <i class="fa fa-print"> Print</i>
                    </button>
                </div>

            </div>
        </div>
        <!-- End Page Header -->
        <div class="printTable2">
            <table class="transaction-history d-none">
                <thead>
                <tr>
                    <th scope="col" class="border-0">#</th>
                    <th scope="col" class="border-0">Booking No</th>
                    <th scope="col" class="border-0">Room Type</th>
                    <th scope="col" class="border-0">Booking From</th>
                    <th scope="col" class="border-0">Booking To</th>
                    <th scope="col" class="border-0">Qty</th>
                    <th scope="col" class="border-0">Amount</th>
                    <th scope="col" class="border-0">Status</th>
                    <th scope="col" class="border-0">Action</th>
                </tr>
                </thead>
                <tbody>
                @php $sl=1 @endphp
                @foreach($booking as $bookings)
                    <tr>
                        <td>{{$sl++}}</td>
                        <td>{{$bookings->booking_no}}</td>
                        <td>{{$bookings->room_type->room_type}}</td>
                        <td>{{$bookings->booking_from}}</td>
                        <td>{{$bookings->booking_to}}</td>
                        <td>{{$bookings->room_qty}}</td>
                        <td>{{$bookings->total_amount}}</td>
                        <td>
                            @if($bookings->status == 1)
                                <span class="badge badge-primary">Booked</span>
                            @else
                                <span class="badge badge-secondary">Request</span>
                            @endif
                        </td>
                        <td>
                            <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                                <button type="button" class="btn btn-white edit"
                                        href="{{route('booking-request.edit',$bookings->id,'edit')}}">
                                    <i class="fas fa-eye"></i>
                                </button>
                                @if($bookings->status == 0)
                                    <button type="button" class="btn btn-white save"
                                            id="{{$bookings->id}}">
                                        <i class="fas fa-check mr-1"></i>
                                    </button>
                                    <button type="button" class="btn btn-white delete"
                                            href="{{route('booking.request.delete',$bookings->id)}}">
                                        <i class="material-icons">&#xE872;</i>
                                    </button>
                                @else
                                    <button type="button" id="{{$bookings->id}}"
                                            class="btn btn-sm btn-white reject"><i
                                            class="fas fa-times mr-1"></i>
                                    </button>
                                @endif
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- End Transaction History Table -->
    </div>
@endsection
@push('style')
    <link rel="stylesheet" href="{{asset('assets/styles/responsive.dataTables.min.css')}}"/>
@endpush
@push('script')
    <script src="{{asset('assets/scripts/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/scripts/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/scripts/app/app-transaction-history.1.3.1.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="{{asset('assets/printThis.js')}}"></script>

    <script>
        $('.printMe').on("click", function () {
            $('.printTable2').printThis({
                base: "https://jasonday.github.io/printThis/"
            });
        });

        $('.delete').click(function (e) {
            e.preventDefault();
            let linkURL = $(this).attr("href");
            Swal.fire({
                title: "Sure want to delete?",
                text: "If you click 'OK' file will be deleted",
                type: "warning",
                showCancelButton: true
            }, function () {
                window.location.href = linkURL;
            });
        });

        $('.edit').click(function (e) {
            let linkURL = $(this).attr("href");
            window.location.href = linkURL;
        });

        $('.save').click(function (e) {
            e.preventDefault();
            let id = $(this).attr("id");


            swal.fire({
                title: 'Are you sure?',
                text: "You want to confirm this booking! Select the payment method",
                type: 'warning',
                input: 'select',
                inputOptions:
                    {
                        @foreach($payment as $payments)
                        '{{$payments->id}}': '{{$payments->name}}',
                        @endforeach
                    },
                inputPlaceholder: 'Select Payment Method',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, book it!',
                showLoaderOnConfirm: true,
                preConfirm: function (value) {
                    if (value == '') {
                        swal.showValidationMessage(
                            'You need to select Payment Method'
                        )
                    }
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {

                var url = '{{ route("booking.accept",[":id",":data"]) }}';
                url = url.replace(':id',result.value);
                url = url.replace(':data',id);
                window.location.href = url;
            })


        });

        $('.reject').click(function (e) {
            e.preventDefault();
            let id = $(this).attr("id");
            Swal.fire({
                title: "Sure want to reject?",
                text: "If you click 'OK' booking rejected",
                type: "warning",
                showCancelButton: true
            }).then((result) => {
                if (result.value) {
                    var url = '{{ route("booking.reject",":id") }}';
                    url = url.replace(':id',id);
                    window.location.href = url;
                }
            })


        });

    </script>
@endpush
