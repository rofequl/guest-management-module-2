@extends('layout.app')
@section('title','Army Golf Club | Department Management')
@section('content')
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <i class="fa fa-check mx-2"></i>
                <strong>Error!</strong> {{$error}}!
            </div>
        @endforeach
    @endif
    @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <i class="fa fa-check mx-2"></i>
            <strong>Success!</strong> {{ session()->get('message') }}!
        </div>
    @endif
    <div class="main-content-container container-fluid px-4 mb-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-6 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">ROOM SETUP</span>
                <h3 class="page-title">Room Information</h3>
            </div>
            <div class="col-12 col-sm-6 d-flex align-items-center">
                <div class="d-inline-flex mb-sm-0 mx-auto ml-sm-auto mr-sm-0" role="group" aria-label="Page actions">
                    <a id="add-new-event" role="button" href="#" class="btn btn-primary" data-toggle="modal"
                       data-target="#exampleModal">
                        <i class="material-icons">add</i> New Room Information Add </a>
                </div>
            </div>
        </div>
        <!-- End Page Header -->

        @if(isset($edit))
            <div class="row">
                <div class="col-sm-12 mb-4">
                    <!-- Quick Post -->
                    <div class="card card-small h-100">
                        <div class="card-header border-bottom">
                            <h6 class="m-0">Update Room Information</h6>
                        </div>
                        <div class="card-body d-flex flex-column">
                            <form class="quick-post-form" method="post"
                                  action="{{route('room-setup.update',$edit->id)}}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form-row">
                                    <div class="col-4 form-group">
                                        <select class="form-control" name="room_type_id">
                                            <option value="" disabled>Choose Room Type</option>
                                            @foreach($room_type as $room_types)
                                                <option
                                                    value="{{$room_types->id}}" {{$room_types->id==$edit->room_type_id?'selected':''}}>
                                                    {{$room_types->room_type}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-4 form-group">
                                        <input type="text" name="rent" value="{{$edit->rent}}"
                                               class="form-control"
                                               placeholder="Rent" required>
                                    </div>
                                    <div class="col-4 form-group">
                                        <input type="text" name="qty" value="{{$edit->qty}}"
                                               class="form-control"
                                               placeholder="Qty" required>
                                    </div>
                                    <div class="col-4 form-group">
                                        <textarea style="min-height: 125px;" id="userBio" name="details"
                                                  placeholder="Room Details" class="form-control">{{$edit->details}}</textarea>
                                    </div>
                                    <div class="col-4 custom-file">
                                        <input type="file" name="image" class="custom-file-input" id="customFile">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <button type="submit" class="btn btn-accent">Update</button>
                                    <a href="{{route('room-setup.index')}}" role="button" class="btn btn-success mx-2">Close</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- End Quick Post -->
                </div>
            </div>
        @endif
        <table class="transaction-history d-none">
            <thead>
            <tr>
                <th scope="col" class="border-0">#</th>
                <th scope="col" class="border-0">Picture</th>
                <th scope="col" class="border-0">Room Type</th>
                <th scope="col" class="border-0">Room Rent Per Night</th>
                <th scope="col" class="border-0">Room Qty</th>
                <th scope="col" class="border-0">Details</th>
                <th scope="col" class="border-0">Action</th>
            </tr>
            </thead>
            <tbody>
            @php $sl=1 @endphp
            @foreach($room as $rooms)
                <tr>
                    <td>{{$sl++}}</td>
                    <td>
                        <img src="{{asset('storage/room/'.$rooms->image)}}" class="img-thumbnail" width="80px">
                    </td>
                    <td>{{$rooms->room_type->room_type}}</td>
                    <td>{{$rooms->rent}}</td>
                    <td>{{$rooms->qty}}</td>
                    <td>{{$rooms->details}}</td>
                    <td>
                        <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                            <button type="button" class="btn btn-white edit"
                                    href="{{route('room-setup.edit',$rooms->id,'edit')}}">
                                <i class="material-icons">&#xE254;</i>
                            </button>
                            <button type="button" class="btn btn-white delete"
                                    href="{{route('room-setup.delete',$rooms->id)}}">
                                <i class="material-icons">&#xE872;</i>
                            </button>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <!-- End Transaction History Table -->
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form method="post" action="{{route('room-setup.store')}}" autocomplete="off" enctype="multipart/form-data">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Room Information Insert</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @csrf

                        <div class="input-group mb-3">
                            <select class="form-control" name="room_type_id">
                                <option value="" selected="" disabled>Choose Room Type</option>
                                @foreach($room_type as $room_types)
                                    <option
                                        value="{{$room_types->id}}">
                                        {{$room_types->room_type}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <div class="input-group input-group-seamless">
                                    <input type="number" name="rent" min="1"
                                           class="form-control" id="rent" placeholder="Room Rent">
                                    <span class="input-group-append">
                                          <span class="input-group-text">&#2547;</span>
                                        </span>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class="input-group">
                                    <input type="number" name="qty" min="1"
                                           class="form-control" id="qty"
                                           placeholder="Total Room">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-8">
                                    <textarea style="min-height: 125px;" id="userBio" name="details"
                                              placeholder="Room Details" class="form-control"></textarea>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="card card-body p-2" onclick="chooseFile()" id="previewImage">
                                    <div class="mx-auto d-block">
                                        <i class="fas fa-cloud-upload-alt fa-3x"></i><br>
                                        Add a Room Image
                                    </div>
                                </div>
                                <input type="file" name="image" class="ImageUpload d-none">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('style')
    <link rel="stylesheet" href="{{asset('assets/styles/responsive.dataTables.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/sweetalert/sweetalert.css')}}"/>
@endpush
@push('script')
    <script src="{{asset('assets/scripts/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/scripts/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/scripts/app/app-transaction-history.1.3.1.min.js')}}"></script>
    <script src="{{asset('assets/sweetalert/sweetalert.js')}}"></script>

    <script>
        $('.delete').click(function (e) {
            e.preventDefault();
            let linkURL = $(this).attr("href");
            swal({
                title: "Sure want to delete?",
                text: "If you click 'OK' file will be deleted",
                type: "warning",
                showCancelButton: true
            }, function () {
                window.location.href = linkURL;
            });
        });

        $('.edit').click(function (e) {
            let linkURL = $(this).attr("href");
            window.location.href = linkURL;
        });

        function chooseFile() {
            $(".ImageUpload").click();
        }
        $(function () {
            $(".ImageUpload").change(function () {
                let file = this.files[0];
                let imagefile = file.type;
                let match = ["image/jpeg", "image/png", "image/jpg"];
                if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                    alert("only jpeg, jpg and png Images type allowed");
                    return false;
                } else {
                    $('#previewImage').html('<img src="" height="100" class="img-thumbnail mx-auto" id="previewLogo">');
                    let reader = new FileReader();
                    reader.onload = imageIsLoaded;
                    reader.readAsDataURL(this.files[0]);
                }
            });
        });
        function imageIsLoaded(e) {
            $('#previewLogo').attr('src', e.target.result);
        }
    </script>
@endpush
