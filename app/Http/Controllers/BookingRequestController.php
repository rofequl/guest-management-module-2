<?php

namespace App\Http\Controllers;

use App\booking;
use App\booking_request;
use App\payment;
use App\room_info;
use Carbon\Carbon;
use DateTime;
use Session;
use Illuminate\Http\Request;

class BookingRequestController extends Controller
{
    public function index()
    {
        $payment = payment::orderBy('id', 'DESC')->get();
        $booking = booking_request::orderBy('id', 'DESC')->get();
        return view('booking_request', compact('booking', 'payment'));
    }

    public function accept($payment, $id)
    {
        //dd($request->all());

        $booking = booking_request::find($id);

        $date1 = new Carbon(DateTime::createFromFormat('Y-m-d', $booking->booking_from)->format('Y-m-d'));
        $date2 = new Carbon(DateTime::createFromFormat('Y-m-d', $booking->booking_to)->format('Y-m-d'));

        $difference = $date1->diff($date2)->days + 1;

        $room_info = room_info::where('room_type_id', $booking->room_type_id)->first();
        $room_qty = $room_info->qty;

        if ($difference == 1) {
            //$room_qty = $room_qty - booking::whereDate('booking_from', $date1)->get()->sum('room_qty');
            $room_qty = $room_qty - booking::whereDate('booking_from', '<=', $date1)->where('room_type_id', $booking->room_type_id)
                    ->whereDate('booking_to', '>=', $date1)->get()->sum('room_qty');
        } else {
            $room_qty = $room_qty - booking::whereDate('booking_from', '>=', $date1)->where('room_type_id', $booking->room_type_id)
                    ->whereDate('booking_to', '<=', $date2)->get()->sum('room_qty');
            $room_qty = $room_qty - booking::whereDate('booking_from', '<=', $date2)->where('room_type_id', $booking->room_type_id)
                    ->whereDate('booking_to', '>', $date2)->get()->sum('room_qty');
            $room_qty = $room_qty - booking::whereDate('booking_from', '<', $date1)->whereDate('booking_to', '>', $date1)->where('room_type_id', $booking->room_type_id)
                    ->whereDate('booking_to', '<', $date2)->get()->sum('room_qty');
        }
        if ($room_qty < $booking->room_qty) {
            return ['status' => 'error', 'room_qty' => 'There are only ' . $room_qty . ' room are available'];
        }

        $insert = new booking();
        $insert->date = $booking->date;
        $insert->booking_no = $booking->booking_no;
        $insert->room_type_id = $booking->room_type_id;
        $insert->member_id = $booking->member_id;
        $insert->name = $booking->name;
        $insert->cell_no = $booking->cell_no;
        $insert->email = $booking->email;
        $insert->address = $booking->address;
        $insert->purpose = $booking->purpose;
        $insert->no_guest = $booking->no_guest;
        $insert->booking_from = $date1;
        $insert->booking_to = $date2;
        $insert->room_rent = $booking->room_rent;
        $insert->room_qty = $booking->room_qty;
        $insert->total_amount = $booking->total_amount;
        $insert->payment_mood = $payment;
        $insert->save();

        $booking->status = 1;
        $booking->payment_mood = $payment;
        $booking->save();

        Session::flash('message', 'Booking request accept successfully');
        return redirect('booking-request');
    }

    public function store(Request $request)
    {
        //
    }

    public function edit($id)
    {
        $edit = booking_request::findOrFail($id);
        return view('booking_view', compact('edit'));
    }

    public function reject($id)
    {
        $booking = booking_request::find($id);
        $accept = booking::where('booking_no', $booking->booking_no)->first();
        $accept->delete();

        $booking->status = 0;
        $booking->payment_mood = '';
        $booking->save();

        Session::flash('message', 'Booking reject successfully');
        return redirect('booking-request');
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $department = booking_request::findOrFail($id);
        $department->delete();

        Session::flash('message', 'Booking request delete successfully');
        return redirect('booking-request');
    }
}
