<?php

namespace App\Http\Controllers\API;

use App\booking;
use App\booking_request;
use App\Http\Controllers\Controller;
use App\payment;
use App\room_info;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DateTime;
use Session;

class FrontendController extends Controller
{
    public function RoomInfo()
    {
        return room_info::with('room_type')->select('id','room_type_id')->latest()->get();
    }

    public function index()
    {
        return booking::with('room_type')->latest()->paginate(10);
    }

    public function PaymentAll()
    {
        return payment::all();
    }

    public function show($id)
    {
        return room_info::with('room_type')->where('room_type_id',$id)->first();
    }

    public function onlineBookStore(Request $request)
    {

        $date = new Carbon(DateTime::createFromFormat('d-m-Y', $request->date)->format('Y-m-d'));
        $date1 = new Carbon(DateTime::createFromFormat('d-m-Y', $request->booking_from)->format('Y-m-d'));
        $date2 = new Carbon(DateTime::createFromFormat('d-m-Y', $request->booking_to)->format('Y-m-d'));

        $insert = new booking_request();
        $insert->date = $date;
        $insert->booking_no = $request->booking_no;
        $insert->room_type_id = $request->room_type_id;
        $insert->member_id = $request->member_id;
        $insert->name = $request->name;
        $insert->cell_no = $request->cell_no;
        $insert->email = $request->email;
        $insert->address = $request->address;
        $insert->purpose = $request->purpose;
        $insert->no_guest = $request->no_guest;
        $insert->booking_from = $date1;
        $insert->booking_to = $date2;
        $insert->room_rent = $request->room_rent;
        $insert->room_qty = $request->room_qty;
        $insert->total_amount = $request->total_amount;
        $insert->payment_mood = $request->payment_mood;
        $insert->save();

        return "Booking request successfull";

    }

    public function search_book($id)
    {
        $department = booking_request::where('booking_no',$id)->first();
        if($department){
            $result = ['result'=>'success', 'member_data'=>$department, 'message'=>'Data found!'];
            return response()->json($result);
        }else{
            $result = ['result'=>'failed', 'message'=>'Data not found!'];
            return response()->json($result);
        }
    }
}
