<?php

namespace App\Http\Controllers;

use App\credit;
use App\debit;
use Session;
use App\payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function index()
    {
        $payment = payment::orderBy('id', 'DESC')->get();
        return view('payment', compact('payment'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'Required|max:191|unique:payments,name,',
        ]);

        $insert = new payment();
        $insert->name = $request->name;
        $insert->save();

        Session::flash('message', 'Payment system add successfully');
        return redirect('payment');
    }


    public function edit($id)
    {
        $edit = payment::findOrFail($id);
        $payment = payment::orderBy('id', 'DESC')->get();
        return view('payment', compact('payment', 'edit'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'Required|max:191|unique:payments,name,' . $id,
        ]);

        $insert = payment::findOrFail($id);
        $insert->name = $request->name;
        $insert->save();

        Session::flash('message', 'Payment system update successfully');
        return redirect('payment');
    }

    public function destroy($id)
    {
        $payment = debit::where('payment_id', $id)->first();
        $payment2 = credit::where('payment_id', $id)->first();
        if ($payment || $payment2) {
            return redirect()->back()->withErrors(['message' => ['Payment system already use another option']]);
        }

        $department = payment::findOrFail($id);
        $department->delete();
        Session::flash('message', 'Payment system delete successfully');
        return redirect('payment');
    }
}
