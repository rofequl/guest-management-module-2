<?php

namespace App\Http\Controllers;

use App\room_info;
use Session;
use App\room_type;
use Illuminate\Http\Request;

class RoomSetupController extends Controller
{
    public function index()
    {
        $room_type = room_type::orderBy('id', 'DESC')->get();
        $room = room_info::orderBy('id', 'DESC')->get();
        return view('room_setup', compact('room', 'room_type'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'room_type_id' => 'Required|max:191|unique:room_infos,room_type_id,',
            'rent' => 'Required|integer|digits_between:1,10',
            'qty' => 'Required|integer|digits_between:1,10',
            'details' => 'Required|regex:/^[a-zA-Z0-9 -.@&]+$/u',
            'image' => 'required',
        ]);

        //dd($request->all());
        $insert = new room_info();
        $insert->room_type_id = $request->room_type_id;
        $insert->rent = $request->rent;
        $insert->qty = $request->qty;
        $insert->details = $request->details;

        if ($request->image) {
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileStore3 = rand(10, 100) . time() . "." . $extension;
            $request->file('image')->storeAs('public/room', $fileStore3);
            $insert->image = $fileStore3;
        }
        $insert->save();

        Session::flash('message', 'Room setup successfully');
        return redirect('room-setup');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $edit = room_info::findOrFail($id);
        $room_type = room_type::orderBy('id', 'DESC')->get();
        $room = room_info::orderBy('id', 'DESC')->get();
        return view('room_setup', compact('room_type', 'room', 'edit'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'room_type_id' => 'Required|max:191|unique:room_infos,room_type_id,' . $id,
            'rent' => 'Required|integer|digits_between:1,10',
            'qty' => 'Required|integer|digits_between:1,10',
            'details' => 'Required|regex:/^[a-zA-Z0-9 -.@&]+$/u',
        ]);

        $insert = room_info::findOrFail($id);
        $insert->room_type_id = $request->room_type_id;
        $insert->rent = $request->rent;
        $insert->qty = $request->qty;
        $insert->details = $request->details;

        if ($request->image) {
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileStore3 = rand(10, 100) . time() . "." . $extension;
            $request->file('image')->storeAs('public/room', $fileStore3);
            $insert->image = $fileStore3;
        }
        $insert->save();

        Session::flash('message', 'Room setup update successfully');
        return redirect('room-setup');
    }

    public function destroy($id)
    {
        $dep = room_info::findOrFail($id);
        $dep->delete();
        Session::flash('message', 'Room setup delete successfully');
        return redirect('room-setup');
    }
}
