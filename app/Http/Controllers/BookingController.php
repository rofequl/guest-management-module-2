<?php

namespace App\Http\Controllers;

use App\booking_request;
use App\credit;
use Carbon\Carbon;
use DateTime;
use Session;
use App\booking;
use App\payment;
use App\room_info;
use App\room_type;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    public function index()
    {
        $booking = booking::orderBy('id', 'DESC')->get();
        return view('booking', compact('booking'));
    }

    public function NewBook()
    {
        $booking_id = booking::orderBy('id', 'DESC')->first()->id + 1;
        $booking_id = rand(100, 999) . $booking_id;
        $payment = payment::orderBy('id', 'DESC')->get();
        $room_type = room_type::orderBy('id', 'DESC')->get();
        return view('new_booking', compact('booking_id', 'payment', 'room_type'));
    }

    public function RoomRent(Request $request)
    {
        return room_info::with('room_type')->where('room_type_id', $request->room_type_id)->first();
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'date' => "Required",
            'booking_no' => 'Required|integer|unique:bookings,booking_no,',
            'room_type_id' => 'Required|integer',
            'name' => 'Required|max:191',
            'cell_no' => 'Required|max:191',
            'email' => 'Required|max:191',
            'address' => 'Required',
            'purpose' => 'Required|max:191',
            'no_guest' => 'Required|integer|digits_between:1,11',
            'booking_from' => "Required",
            'booking_to' => "Required",
            'room_rent' => 'Required|integer|digits_between:1,11',
            'room_qty' => 'Required|integer|digits_between:1,11',
            'total_amount' => 'Required|integer|digits_between:1,11',
            'payment_mood' => 'Required|integer',
        ]);

        if ($request->member_edit == 0) {
            $this->validate($request, [
                'member_id' => 'Required|max:191',
            ]);
        }

        $date = new Carbon(DateTime::createFromFormat('d-m-Y', $request->date)->format('Y-m-d'));
        $date1 = new Carbon(DateTime::createFromFormat('d-m-Y', $request->booking_from)->format('Y-m-d'));
        $date2 = new Carbon(DateTime::createFromFormat('d-m-Y', $request->booking_to)->format('Y-m-d'));
        $difference = $date1->diff($date2)->days + 1;

        $room_info = room_info::where('room_type_id', $request->room_type_id)->first();
        $room_qty = $room_info->qty;

        if ($difference == 1) {
            //$room_qty = $room_qty - booking::whereDate('booking_from', $date1)->get()->sum('room_qty');
            $room_qty = $room_qty - booking::whereDate('booking_from', '<=', $date1)->where('room_type_id', $request->room_type_id)
                    ->whereDate('booking_to', '>=', $date1)->get()->sum('room_qty');
        } else {
            $room_qty = $room_qty - booking::whereDate('booking_from', '>=', $date1)->where('room_type_id', $request->room_type_id)
                    ->whereDate('booking_to', '<=', $date2)->get()->sum('room_qty');
            $room_qty = $room_qty - booking::whereDate('booking_from', '<=', $date2)->where('room_type_id', $request->room_type_id)
                    ->whereDate('booking_to', '>', $date2)->get()->sum('room_qty');
            $room_qty = $room_qty - booking::whereDate('booking_from', '<', $date1)->whereDate('booking_to', '>', $date1)->where('room_type_id', $request->room_type_id)
                    ->whereDate('booking_to', '<', $date2)->get()->sum('room_qty');
        }
        if ($room_qty < $request->room_qty) {
            return redirect()->back()->withErrors(['message' => ['There are only ' . $room_qty . ' room are available']]);
        }

        $insert = new booking();
        $insert->date = $date;
        $insert->booking_no = $request->booking_no;
        $insert->room_type_id = $request->room_type_id;
        $insert->member_id = $request->member_id;
        $insert->name = $request->name;
        $insert->cell_no = $request->cell_no;
        $insert->email = $request->email;
        $insert->address = $request->address;
        $insert->purpose = $request->purpose;
        $insert->no_guest = $request->no_guest;
        $insert->booking_from = $date1;
        $insert->booking_to = $date2;
        $insert->room_rent = $request->room_rent;
        $insert->room_qty = $request->room_qty;
        $insert->total_amount = $request->total_amount;
        $insert->payment_mood = $request->payment_mood;
        $insert->save();

        $credit = credit::orderBy('id', 'DESC')->first();
        if ($credit) {
            $creditId = $credit->id;
        } else {
            $creditId = 0;
        }

        $credit1 = new credit();
        $credit1->date = $date;
        $credit1->voucher_no = rand(100, 999) . '' . $creditId;
        $credit1->income_source_id = 0;
        $credit1->amount = $request->total_amount;
        $credit1->payment_id = $request->payment_mood;
        $credit1->save();


        Session::flash('message', 'Booking successfully');
        return redirect('new-booking');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $edit = booking::findOrFail($id);
        return view('booking_view', compact('edit'));
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function schedule()
    {
        $booking = booking::orderBy('id', 'DESC')->get();
        return view('booking_schedule',compact('booking'));
    }

    public function destroy($id)
    {
        $department = booking::findOrFail($id);
        $booking = booking_request::where('booking_no',$department->booking_no)->first();
        if ($booking){
            $booking->status = 0;
            $booking->payment_mood = '';
            $booking->save();
        }
        $department->delete();
        Session::flash('message', 'Booking delete successfully');
        return redirect('booking');
    }
}
