<?php

namespace App\Http\Controllers;

use App\room_info;
use App\room_type;
use Session;
use Illuminate\Http\Request;

class RoomTypeController extends Controller
{
    public function index()
    {
        $room_type = room_type::orderBy('id', 'DESC')->get();
        return view('room_type', compact('room_type'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'Required|max:191|unique:room_types,room_type,',
        ]);

        $insert = new room_type();
        $insert->room_type = $request->name;
        $insert->save();

        Session::flash('message', 'Room type add successfully');
        return redirect('room-type');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $edit = room_type::findOrFail($id);
        $room_type = room_type::orderBy('id', 'DESC')->get();
        return view('room_type', compact('room_type', 'edit'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'Required|max:191|unique:room_types,room_type,' . $id,
        ]);

        $insert = room_type::findOrFail($id);
        $insert->room_type = $request->name;
        $insert->save();

        Session::flash('message', 'Room type update successfully');
        return redirect('room-type');
    }

    public function destroy($id)
    {
        $room = room_info::where('room_type_id',$id)->first();
        if ($room){
            return redirect()->back()->withErrors(['message' => ['Room type already use room info']]);
        }


        $department = room_type::findOrFail($id);
        $department->delete();
        Session::flash('message', 'Room type delete successfully');
        return redirect('room-type');
    }
}
