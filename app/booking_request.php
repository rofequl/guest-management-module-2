<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class booking_request extends Model
{
    public function room_type(){
        return $this->belongsTo(room_type::class);
    }

    public function payment(){
        return $this->belongsTo(payment::class,'payment_mood');
    }

    public function room_info(){
        return $this->belongsTo(room_info::class,'room_type_id','room_type_id');
    }
}
